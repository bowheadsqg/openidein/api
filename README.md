# Open Idein API

Open Idein API provides all required services to [openidein-ui](https://bitbucket.org/bowheadsqg/openidein-ui).


## Web Framework

We use [Gin](https://gin-gonic.github.io/gin/) to eliminate boilerplate code and to ease routing + validation.


## Install Go Dependencies

First, make sure you have installed [Go v1.11+](https://golang.org/) and created a standard workspace (with valid `$GOPATH`).

Install *Glide* either by running `curl https://glide.sh/get | sh` or by some other installation method explained [here](https://glide.sh/).

*Glide* is our preferred Go package manager. Install app dependencies with:

    glide install

You will see a newly created directory named `vendor` if it's your first time running the command.


## Code Format


## Code Documentation


## Static Analysis

### Overall Check

### Pedantic Check

### Security Check

### Deadlock Check
