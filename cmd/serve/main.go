package main

import (
	"flag"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/gocql/gocql"
	"github.com/spf13/viper"
	"gitlab.com/bowheadsqg/openidein/api/pkg/core"
	web "gitlab.com/bowheadsqg/openidein/api/pkg/core/gin"
	db "gitlab.com/bowheadsqg/openidein/api/pkg/core/gocql"
	config "gitlab.com/bowheadsqg/openidein/api/pkg/core/viper"
)

func setupRouter() *gin.Engine {
	r := gin.New()

	// Middlewares
	r.Use(gin.Logger())
	r.Use(gin.Recovery())
	r.Use(web.Doctor())
	r.Use(web.CORSifier())
	r.Use(web.Responder())
	r.Use(web.Validator())

	// User Routes
	userCtrl := web.NewUserController()
	r.POST("/users", userCtrl.CreateUser)

	// Token Routes
	tokenCtrl := web.NewTokenController()
	r.POST("/accessTokens", tokenCtrl.GenerateAccessToken)

	// Authorized Routes
	authorized := r.Group("/")
	authorized.Use(web.Authorizer())
	{
		authorized.POST("/testAuth", func(c *gin.Context) {
			userClaims := c.MustGet("userClaims").(*core.UserClaims)
			c.Set("response", gin.H{
				"status":  http.StatusOK,
				"data":    userClaims,
				"message": "You are authorized!",
			})
		})
	}

	return r
}

func main() {
	// Declare & parse command-line flags.
	cfgFlag := flag.String("cfg", "", "configuration file path")
	flag.Parse()

	// Initialize config.
	cfg := config.New(viper.New(), *cfgFlag)
	config.SetDefault(cfg)

	// Initialize log stream.
	web.InitLog()

	// Initialize Cassandra cluster.
	gc := gocql.NewCluster(cfg.CassandraClusterIPs()...)
	gc.Keyspace = cfg.CassandraKeyspace()
	cluster := db.NewCluster(gc)
	db.SetDefaultCluster(cluster)

	r := setupRouter()
	r.Run(":" + cfg.Port())
}
