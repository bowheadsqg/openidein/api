package main

import (
	"crypto/rand"
	"encoding/base64"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strconv"
)

func main() {
	if len(os.Args) != 3 {
		log.Fatal("required arguments: <key size (in bytes)> <key name>")
		return
	}
	size, err := strconv.Atoi(os.Args[1])
	if err != nil {
		log.Fatal(err)
		return
	}
	name := os.Args[2]

	key := make([]byte, size)
	if _, err := rand.Read(key); err != nil {
		log.Fatal(err)
		return
	}

	b64Key := base64.StdEncoding.EncodeToString(key)
	path := fmt.Sprintf(`private/%s`, name)
	if err := ioutil.WriteFile(path, []byte(b64Key), os.FileMode(0600)); err != nil {
		log.Fatal(err)
		return
	}
}
