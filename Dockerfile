# Build the main binary with tools.
FROM golang:1.11.2-stretch as build

RUN apt-get update && apt-get install -y --no-install-recommends apt-utils
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
        upx-ucl \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /go/src/gitlab.com/bowheadsqg/openidein/api
COPY glide.yaml glide.lock ./
RUN curl -sS https://glide.sh/get | sh && glide install
COPY . .
RUN GOBIN=/go/bin GOOS=linux go install -v -ldflags="-s -w" cmd/serve/main.go \
    && upx --lzma /go/bin/main

# Abandon the tools.
FROM scratch

COPY --from=build /go/bin/main /bin/serve
CMD ["/bin/serve"]
