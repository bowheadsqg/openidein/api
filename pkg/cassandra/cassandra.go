package cassandra

// Central place for queries
const (
	InsertIntoUsersByUsername = `INSERT INTO users_by_username (username, password,` +
		`user_id) VALUES (?, ?, ?) IF NOT EXISTS`

	InsertIntoUsersByEmailAddr = `INSERT INTO users_by_email_addr (email_addr, user_id)` +
		`VALUES (?, ?) IF NOT EXISTS`

	InsertIntoUsers = `INSERT INTO users (user_id, full_name, email_addr, username, password)` +
		`VALUES (?, ?, ?, ?, ?) IF NOT EXISTS`

	SelectOneFromUsersByUsername = `SELECT password, user_id FROM users_by_username` +
		`WHERE username = ? LIMIT 1`
)

type Cluster interface {
	CreateSession() (Session, error)
}

type Session interface {
	Query(string, ...interface{}) Query
	Close()
}

type Query interface {
	Consistency(Consistency) Query
	Exec() error
	Scan(...interface{}) error
}

type Consistency uint16

const (
	One         Consistency = 0x01
	Quorum      Consistency = 0x04
	All         Consistency = 0x05
	LocalQuorum Consistency = 0x06
)
