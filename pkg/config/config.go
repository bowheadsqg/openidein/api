// Package config provides interfaces and constants for config implementations.
package config

// Application-wide Config Constants
const (
	EnvPrefix = "OPENIDEIN"
)

type Config interface {
	Port() string
	LogPath() string
	TokenKey() string
	CassandraClusterIPs() []string
	CassandraKeyspace() string
}
