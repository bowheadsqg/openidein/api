package mock

import "gitlab.com/bowheadsqg/openidein/api/pkg/cassandra"

// Cluster is a mock implementation of cassandra.Cluster.
type Cluster struct {
	CreateSessionFn      func() (cassandra.Session, error)
	CreateSessionInvoked bool
}

func NewCluster() *Cluster {
	return &Cluster{}
}

func (c *Cluster) CreateSession() (cassandra.Session, error) {
	c.CreateSessionInvoked = true
	return c.CreateSessionFn()
}

// Session is a mock implementation of cassandra.Session.
type Session struct {
	CloseFn      func()
	CloseInvoked bool

	QueryFn      func(string, ...interface{}) cassandra.Query
	QueryInvoked bool
}

func NewSession() *Session {
	return &Session{}
}

func (s *Session) Close() {
	s.CloseInvoked = true
	s.CloseFn()
}

func (s *Session) Query(stmt string, values ...interface{}) cassandra.Query {
	s.QueryInvoked = true
	return s.QueryFn(stmt, values...)
}

// Query is a mock implementation of cassandra.Query
type Query struct {
	ConsistencyFn      func(cassandra.Consistency) cassandra.Query
	ConsistencyInvoked bool

	ExecFn      func() error
	ExecInvoked bool

	ScanFn      func(...interface{}) error
	ScanInvoked bool
}

func NewQuery() *Query {
	return &Query{}
}

func (q *Query) Consistency(c cassandra.Consistency) cassandra.Query {
	q.ConsistencyInvoked = true
	return q.ConsistencyFn(c)
}

func (q *Query) Exec() error {
	q.ExecInvoked = true
	return q.ExecFn()
}

func (q *Query) Scan(dest ...interface{}) error {
	q.ScanInvoked = true
	return q.ScanFn(dest...)
}
