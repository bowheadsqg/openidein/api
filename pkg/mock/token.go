package mock

import "gitlab.com/bowheadsqg/openidein/api/pkg/core"

// TokenPayload is a mock implementation of core.TokenPayload interface.
type TokenPayload struct {
	ValueFn      func() interface{}
	ValueInvoked bool

	FromStringFn      func(string) error
	FromStringInvoked bool

	ToStringFn      func() (string, error)
	ToStringInvoked bool
}

func NewTokenPayload() *TokenPayload {
	return &TokenPayload{}
}

func (p *TokenPayload) Value() interface{} {
	p.ValueInvoked = true
	return p.ValueFn()
}

func (p *TokenPayload) FromString(s string) error {
	p.FromStringInvoked = true
	return p.FromStringFn(s)
}

func (p *TokenPayload) ToString() (string, error) {
	p.ToStringInvoked = true
	return p.ToStringFn()
}

// TokenKeyReader is a mock implementation of core.TokenKeyReader.
type TokenKeyReader struct {
	ReadFn      func() (core.TokenKey, error)
	ReadInvoked bool
}

func NewTokenKeyReader() *TokenKeyReader {
	return &TokenKeyReader{}
}

func (r *TokenKeyReader) Read() (core.TokenKey, error) {
	r.ReadInvoked = true
	return r.ReadFn()
}
