package json_test

import (
	"testing"

	"gitlab.com/bowheadsqg/openidein/api/pkg/core/json"
	"gotest.tools/assert"
)

type TestType struct {
	A string `json:"a"`
	B int    `json:"b"`
}

func TestPayloadFromString(t *testing.T) {
	payload := json.NewTokenPayload(new(TestType))
	err := payload.FromString(`{"a":"test","b":99}`)
	assert.NilError(t, err)
	assert.Equal(t, *payload.Value().(*TestType), TestType{A: "test", B: 99})
}

func TestPayloadFromStringNonPointerVal(t *testing.T) {
	defer func() {
		r := recover()
		assert.Assert(t, r != nil)
		assert.Error(t, r.(error), "token payload value must be a pointer")
	}()
	_ = json.NewTokenPayload(TestType{})
}

func TestPayloadToString(t *testing.T) {
	payload := json.NewTokenPayload(&TestType{A: "test", B: 99})
	jsonStr, err := payload.ToString()
	assert.NilError(t, err)
	assert.Equal(t, jsonStr, `{"a":"test","b":99}`)
}

func TestPayloadInvalidTypeToString(t *testing.T) {
	ch := make(chan int)
	payload := json.NewTokenPayload(&ch)
	_, err := payload.ToString()
	assert.Assert(t, err != nil)
}
