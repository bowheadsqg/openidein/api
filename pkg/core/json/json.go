// Package json implements the representation of application artifacts in JSON.
package json

import (
	"encoding/json"
	"errors"
	"reflect"
)

/*
 * Token
 */

// TokenPayload implements core.TokenPayload interface.
type TokenPayload struct {
	// val can be a pointer to anything but a channel/complex/func value.
	val interface{}
}

// NewTokenPayload returns a new token payload which must wrap a pointer.
func NewTokenPayload(val interface{}) *TokenPayload {
	if reflect.ValueOf(val).Kind() != reflect.Ptr {
		panic(errors.New("token payload value must be a pointer"))
	}
	return &TokenPayload{val}
}

func (p *TokenPayload) Value() interface{} {
	return p.val
}

// FromString unmarshals a JSON string into the payload's underlying value.
func (p *TokenPayload) FromString(s string) error {
	return json.Unmarshal([]byte(s), p.val)
}

// ToString marshals the payload's underlying value into a JSON string.
func (p *TokenPayload) ToString() (string, error) {
	res, err := json.Marshal(p.val)
	if err != nil {
		return "", err
	}
	return string(res), nil
}
