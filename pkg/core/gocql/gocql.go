// Package gocql implements data services backed by Cassandra DBMS.
package gocql

import (
	"errors"

	"github.com/gocql/gocql"
	"gitlab.com/bowheadsqg/openidein/api/pkg/cassandra"
)

var defaultCluster cassandra.Cluster

// DefaultCluster returns the default cluster.
func DefaultCluster() cassandra.Cluster {
	return defaultCluster
}

// SetDefaultCluster replaces the default cluster.
func SetDefaultCluster(c cassandra.Cluster) {
	defaultCluster = c
}

// Cluster wraps gocql.ClusterConfig.
type Cluster struct {
	*gocql.ClusterConfig
}

// NewCluster returns a newly configured Cassandra cluster.
func NewCluster(c *gocql.ClusterConfig) cassandra.Cluster {
	return &Cluster{c}
}

// CreateSession tries to create a new Cassandra cluster session.
func (c *Cluster) CreateSession() (cassandra.Session, error) {
	session, err := c.ClusterConfig.CreateSession()
	if err != nil {
		return nil, err
	}
	return NewSession(session), nil
}

// Session wraps gocql.Session.
type Session struct {
	*gocql.Session
}

// NewSession returns a new Cassandra cluster session.
func NewSession(s *gocql.Session) cassandra.Session {
	return &Session{s}
}

// Query constructs a new query that can be chained with more statements.
func (s *Session) Query(stmt string, values ...interface{}) cassandra.Query {
	return NewQuery(s.Session.Query(stmt, values...))
}

// Query wraps gocql.Query.
type Query struct {
	*gocql.Query
}

// NewQuery returns a new Cassandra query.
func NewQuery(q *gocql.Query) cassandra.Query {
	return &Query{q}
}

// Consistency configures the consistency level of a specific query.
func (q *Query) Consistency(level cassandra.Consistency) cassandra.Query {
	// Map cassandra.Consistency to gocql.Consistency.
	var c gocql.Consistency
	switch level {
	case cassandra.One:
		c = gocql.One
	case cassandra.Quorum:
		c = gocql.Quorum
	case cassandra.LocalQuorum:
		c = gocql.LocalQuorum
	case cassandra.All:
		c = gocql.All
	default:
		panic(errors.New("unrecognized Cassandra query consistency level"))
	}
	return NewQuery(q.Query.Consistency(c))
}
