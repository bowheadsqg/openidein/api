package gocql_test

import (
	"testing"

	"github.com/gocql/gocql"
	"gitlab.com/bowheadsqg/openidein/api/pkg/cassandra"
	db "gitlab.com/bowheadsqg/openidein/api/pkg/core/gocql"
	"gitlab.com/bowheadsqg/openidein/api/pkg/mock"
	"gotest.tools/assert"
)

func TestSetGetDefaultCluster(t *testing.T) {
	c := mock.NewCluster()
	db.SetDefaultCluster(c)
	assert.Equal(t, db.DefaultCluster(), c)
}

func TestSetQueryValidConsistencyLevel(t *testing.T) {
	defer func() {
		assert.Assert(t, recover() == nil)
	}()

	levels := []cassandra.Consistency{
		cassandra.One,
		cassandra.Quorum,
		cassandra.LocalQuorum,
		cassandra.All,
	}
	q := db.NewQuery(&gocql.Query{})
	for _, l := range levels {
		q.Consistency(l)
	}
}

func TestSetQueryInvalidConsistencyLevel(t *testing.T) {
	defer func() {
		assert.Assert(t, recover() != nil)
	}()

	levels := []cassandra.Consistency{
		0x02,
		0x03,
		0x07,
	}
	q := db.NewQuery(&gocql.Query{})
	for _, l := range levels {
		q.Consistency(l)
	}
}
