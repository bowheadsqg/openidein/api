package gocql_test

import (
	"errors"
	"testing"

	"gitlab.com/bowheadsqg/openidein/api/pkg/cassandra"
	"gitlab.com/bowheadsqg/openidein/api/pkg/core"
	"gitlab.com/bowheadsqg/openidein/api/pkg/core/gocql"
	"gitlab.com/bowheadsqg/openidein/api/pkg/mock"
	"gotest.tools/assert"
)

var query *mock.Query
var session *mock.Session
var cluster *mock.Cluster

func setup() func() {
	query = mock.NewQuery()
	query.ConsistencyFn = func(_ cassandra.Consistency) cassandra.Query {
		return query
	}
	query.ExecFn = func() error {
		return nil
	}

	session = mock.NewSession()
	session.CloseFn = func() {}
	session.QueryFn = func(q string, v ...interface{}) cassandra.Query {
		return query
	}

	cluster = mock.NewCluster()
	cluster.CreateSessionFn = func() (cassandra.Session, error) {
		return session, nil
	}

	return func() {
		// tear down
	}
}

func TestCreateUser(t *testing.T) {
	defer setup()()

	var nQueries int
	query.ConsistencyFn = func(_ cassandra.Consistency) cassandra.Query {
		return query
	}
	query.ExecFn = func() error {
		nQueries += 1
		return nil
	}

	var queriesExecuted []string
	session.QueryFn = func(q string, v ...interface{}) cassandra.Query {
		queriesExecuted = append(queriesExecuted, q)
		return query
	}

	expectedID := "d034ff63-1c9f-4c6b-a756-49a9185583ab"
	user := core.NewUser("toto africa", "toto@africa.com", "toto", "africa")
	user.ID = expectedID

	service := gocql.NewUserService(cluster)
	id, err := service.Create(*user)
	assert.NilError(t, err)
	assert.Equal(t, id, expectedID)
	assert.Equal(t, cluster.CreateSessionInvoked, true)
	assert.Equal(t, nQueries, 3)
	assert.DeepEqual(t, queriesExecuted, []string{
		cassandra.InsertIntoUsersByUsername,
		cassandra.InsertIntoUsersByEmailAddr,
		cassandra.InsertIntoUsers,
	})
	assert.Equal(t, session.CloseInvoked, true)
}

func TestCreateUserCreateSessionError(t *testing.T) {
	defer setup()()

	expectedErr := "cannot create session"
	cluster.CreateSessionFn = func() (cassandra.Session, error) {
		return nil, errors.New(expectedErr)
	}

	service := gocql.NewUserService(cluster)
	_, err := service.Create(core.User{})
	assert.Error(t, err, expectedErr)
}

func TestCreateUserGenerateNewUserID(t *testing.T) {
	defer setup()()

	user := core.NewUser("toto africa", "toto@africa.com", "toto", "africa")
	service := gocql.NewUserService(cluster)
	id, err := service.Create(*user)
	assert.NilError(t, err)
	assert.Equal(t, len(id), 36)
}

func TestCreateUserQueryExecErrors(t *testing.T) {
	defer setup()()

	checkedStmts := []string{
		cassandra.InsertIntoUsersByUsername,
		cassandra.InsertIntoUsersByEmailAddr,
		cassandra.InsertIntoUsers,
	}

	for _, checked := range checkedStmts {
		expectedErr := "cannot exec " + checked

		var stmt string
		session.QueryFn = func(q string, _ ...interface{}) cassandra.Query {
			if q == checked {
				stmt = q
			}
			return query
		}
		query.ExecFn = func() error {
			if stmt == checked {
				return errors.New(expectedErr)
			}
			return nil
		}

		service := gocql.NewUserService(cluster)
		_, err := service.Create(core.User{})
		assert.Error(t, err, expectedErr)
	}
}

func TestGetUserCredentials(t *testing.T) {
	defer setup()()

	username, password, userID := "user", "pass", "uuid"
	query.ScanFn = func(dest ...interface{}) error {
		vals := []string{password, userID}
		for i, d := range dest {
			s := d.(*string)
			*s = vals[i]
		}
		return nil
	}

	service := gocql.NewUserCredentialsService(cluster)
	creds, err := service.One(username)
	assert.NilError(t, err)
	assert.DeepEqual(t, creds, core.NewUserCredentials(username, password, userID))
}

func TestGetUserCredentialsCreateSessionError(t *testing.T) {
	defer setup()()

	expectedErr := "cannot create session"
	cluster.CreateSessionFn = func() (cassandra.Session, error) {
		return nil, errors.New(expectedErr)
	}

	service := gocql.NewUserCredentialsService(cluster)
	_, err := service.One("user")
	assert.Error(t, err, expectedErr)
}

func TestGetUserCredentialsQueryScanError(t *testing.T) {
	defer setup()()

	expectedErr := "cannot scan"
	query.ScanFn = func(_ ...interface{}) error {
		return errors.New(expectedErr)
	}

	service := gocql.NewUserCredentialsService(cluster)
	_, err := service.One("user")
	assert.Error(t, err, expectedErr)
}
