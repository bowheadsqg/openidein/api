package gocql

import (
	"github.com/satori/go.uuid"
	"gitlab.com/bowheadsqg/openidein/api/pkg/cassandra"
	"gitlab.com/bowheadsqg/openidein/api/pkg/core"
	"golang.org/x/crypto/bcrypt"
)

// UserService implements core.UserService interface.
type UserService struct {
	cluster cassandra.Cluster
}

// NewUserService returns a new instance of user data service.
func NewUserService(cluster cassandra.Cluster) *UserService {
	return &UserService{cluster}
}

// Create a new user with unique username and email address.
func (s UserService) Create(user core.User) (string, error) {
	session, err := s.cluster.CreateSession()
	if err != nil {
		return "", err
	}
	defer session.Close()

	// Generate user ID only if none is supplied (assumed 36 chars).
	if len(user.ID) != 36 {
		userID, err := uuid.NewV4()
		if err != nil {
			return "", err
		}
		user.ID = userID.String()
	}

	// Hash user's password.
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(user.Password), 12)
	if err != nil {
		return "", err
	}
	user.Password = string(hashedPassword)

	// Only a user who claims its username first gets to claim its email address.
	// TODO: Maybe the claims can use Redis instead, so we can use materialized views.
	if err := session.Query(cassandra.InsertIntoUsersByUsername,
		user.Username,
		user.Password,
		user.ID,
	).Consistency(cassandra.One).Exec(); err != nil {
		return "", err
	}
	if err := session.Query(cassandra.InsertIntoUsersByEmailAddr,
		user.EmailAddr,
		user.ID,
	).Consistency(cassandra.One).Exec(); err != nil {
		return "", err
	}

	// After claming username and email address, the full user data is saved.
	// TODO: Nope, you should send a confirmation email first!
	if err := session.Query(cassandra.InsertIntoUsers,
		user.ID,
		user.FullName,
		user.EmailAddr,
		user.Username,
		user.Password,
	).Consistency(cassandra.One).Exec(); err != nil {
		return "", err
	}

	return user.ID, nil
}

// UserCredentialsService implements core.UserCredentialsService interface.
type UserCredentialsService struct {
	cluster cassandra.Cluster
}

// NewUserCredentialsService returns a new instance of user credentials data service.
func NewUserCredentialsService(cluster cassandra.Cluster) *UserCredentialsService {
	return &UserCredentialsService{cluster}
}

// One retrieves a user credentials instance from the database.
func (s UserCredentialsService) One(username string) (creds *core.UserCredentials, err error) {
	session, err := s.cluster.CreateSession()
	if err != nil {
		return
	}
	defer session.Close()

	var password, userID string
	if err = session.Query(cassandra.SelectOneFromUsersByUsername, username).
		Consistency(cassandra.One).Scan(&password, &userID); err != nil || userID == "" {
		return // either an error or user not found
	}

	creds = core.NewUserCredentials(username, password, userID)
	return
}
