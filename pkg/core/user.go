package core

type User struct {
	ID        string
	FullName  string `json:"fullName" binding:"required"`
	EmailAddr string `json:"emailAddr" binding:"required,email"`
	Username  string `json:"username" binding:"required"`
	Password  string `json:"password" binding:"required"`
}

func NewUser(fullName, emailAddr, username, password string) *User {
	return &User{"", fullName, emailAddr, username, password}
}

type UserService interface {
	Create(User) (string, error)
}

type UserCredentials struct {
	Username string `json:"username" binding:"required"`
	Password string `json:"password" binding:"required"`
	UserID   string
}

func NewUserCredentials(username, password, userID string) *UserCredentials {
	return &UserCredentials{username, password, userID}
}

type UserCredentialsService interface {
	One(username string) (*UserCredentials, error)
}

type UserClaims struct {
	UserID string `json:"userID"`
	Origin string `json:"origin"`
	IPAddr string `json:"ip"`
	Expiry int64  `json:"expiry"`
	Renew  int64  `json:"renew"`
}

func NewUserClaims(userID string, origin string, ipAddr string, expiry int64, renew int64) *UserClaims {
	return &UserClaims{userID, origin, ipAddr, expiry, renew}
}
