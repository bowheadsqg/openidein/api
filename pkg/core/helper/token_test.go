package helper_test

import (
	"os"
	"testing"
	"time"

	"github.com/spf13/viper"
	"gitlab.com/bowheadsqg/openidein/api/pkg/config"
	"gitlab.com/bowheadsqg/openidein/api/pkg/core/helper"
	myconfig "gitlab.com/bowheadsqg/openidein/api/pkg/core/viper"
	"gotest.tools/assert"
)

func TestExtractUserClaimsFromAccessTokenGenerated(t *testing.T) {
	os.Setenv(config.EnvPrefix+"_TOKEN_KEY", "kkiE3y2mp3SS9QIN4eIwHK+tw64nzy/futnCLFJ+zoU=")
	cfg := myconfig.New(viper.New(), "")
	myconfig.SetDefault(cfg)

	userID, origin, clientIP := "d034ff63-1c9f-4c6b-a756-49a9185583ab", "", "127.0.0.1"
	accessToken, _, err := helper.GenAccessToken(userID, origin, clientIP)
	assert.NilError(t, err)
	claims, err := helper.ExtractUserClaims(accessToken)
	assert.NilError(t, err)
	assert.Equal(t, claims.UserID, userID)
	assert.Equal(t, claims.Origin, origin)
	assert.Equal(t, claims.IPAddr, clientIP)

	window, err := time.ParseDuration("168h")
	assert.NilError(t, err)
	assert.Equal(t, claims.Renew-claims.Expiry, int64(window))
}
