package helper

import (
	"time"

	"gitlab.com/bowheadsqg/openidein/api/pkg/core"
	"gitlab.com/bowheadsqg/openidein/api/pkg/core/jose"
	"gitlab.com/bowheadsqg/openidein/api/pkg/core/json"
	config "gitlab.com/bowheadsqg/openidein/api/pkg/core/viper"
)

// ExtractUserClaims decrypts or decodes an access token into a set of user claims.
func ExtractUserClaims(accessToken string) (*core.UserClaims, error) {
	payload := json.NewTokenPayload(new(core.UserClaims))
	token := jose.NewToken(accessToken)
	err := token.Extract(config.NewTokenKeyReader(), payload)
	return payload.Value().(*core.UserClaims), err
}

// GenAccessToken generates a JWT with the supplied claims:
// - the requesting user ID,
// - request origin, and
// - client IP address.
func GenAccessToken(userID string, origin string, clientIP string) (accessToken string,
	validity time.Duration, err error) {

	validity, err = time.ParseDuration("15m")
	if err != nil {
		panic(err)
	}
	expiryTime := time.Now().UTC().Add(validity).UnixNano()

	renewWithin := "24h" // 1 day
	// If the request comes from a non-browser client, token can be renewed within a longer duration.
	// NOTE: No guarantee. Do tell if an attacker can abuse this somehow.
	if origin == "" {
		renewWithin = "168h" // 1 week
	}
	renewalWindow, err := time.ParseDuration(renewWithin)
	if err != nil {
		panic(err)
	}
	renewTime := expiryTime + int64(renewalWindow)

	claims := core.NewUserClaims(userID, origin, clientIP, expiryTime, renewTime)
	protection := jose.NewTokenProtection(json.NewTokenPayload(claims))
	token, err := protection.Do(jose.NewTokenAlgo(), config.NewTokenKeyReader())
	if err != nil {
		return
	}
	accessToken = token.Value()
	return
}
