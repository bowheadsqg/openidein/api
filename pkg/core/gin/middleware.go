package gin

import (
	"fmt"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/gocraft/health"
	"gitlab.com/bowheadsqg/openidein/api/pkg/core/helper"
	config "gitlab.com/bowheadsqg/openidein/api/pkg/core/viper"
	"gopkg.in/go-playground/validator.v8"
)

var Log *health.Stream

func InitLog() {
	path := config.Default().LogPath()
	if err := os.MkdirAll(filepath.Dir(path), 0700); err != nil {
		panic(err)
	}

	logFile, err := os.OpenFile(path, os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0600)
	if err != nil {
		panic(err)
	}

	Log = health.NewStream()
	Log.AddSink(&health.WriterSink{Writer: logFile})
}

func Doctor() gin.HandlerFunc {
	return func(c *gin.Context) {
		job := Log.NewJob(c.Request.URL.Path)
		defer func() {
			if r := recover(); r != nil {
				if err, ok := r.(error); ok {
					job.EventErr("recovered_from_panic", err)
				}
				job.Complete(health.Panic)
				panic(r)
			}
		}()

		c.Set("log", job)
		c.Next()

		s := c.Writer.Status()
		switch {
		case s >= http.StatusContinue && s < http.StatusPermanentRedirect:
			job.Complete(health.Success)
		case s == http.StatusBadRequest:
			job.Complete(health.ValidationError)
		case s >= http.StatusUnauthorized && s <= http.StatusUnavailableForLegalReasons:
			job.Complete(health.Junk)
		default:
			job.Complete(health.Error)
		}
	}
}

func CORSifier() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		c.Writer.Header().Set("Access-Control-Allow-Credentials", "true")
		c.Writer.Header().Set("Access-Control-Allow-Headers",
			"Content-Type, Content-Length, Accept-Encoding, Authorization, "+
				"Accept, Origin, Cache-Control, X-Requested-With")
		c.Writer.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT, DELETE")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(204)
			return
		}
		c.Next()
	}
}

func Responder() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Next()

		// If the request doesn't use default response utility, continue.
		resp, written := c.Get("response")
		if written == false {
			return
		}
		kvs := resp.(gin.H)

		// Always respond with an array of messages.
		var msgs []string
		switch val := kvs["message"].(type) {
		case []string:
			msgs = val
		case string:
			msgs = []string{val}
		default:
			msgs = []string{}
		}
		c.JSON(kvs["status"].(int), gin.H{"data": kvs["data"], "messages": msgs})
	}
}

func Validator() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Next()

		err, exists := c.Get("validationError")
		if exists == false {
			return
		}

		// Collect validation error messages.
		msgs := []string{}
		msg := "Invalid JSON"
		if errs, ok := err.(validator.ValidationErrors); ok == true {
			for _, validationErr := range errs {
				msg = validationMsg(validationErr)
				msgs = append(msgs, msg)
			}
		} else {
			msgs = append(msgs, msg)
		}
		c.Set("response", gin.H{"status": http.StatusBadRequest, "message": msgs})
	}
}

func validationMsg(err *validator.FieldError) string {
	switch err.Tag {
	case "required":
		return fmt.Sprintf("%s is required", err.Name)
	case "email":
		return fmt.Sprintf("'%s' is not a valid email address", err.Value)
	default:
		return fmt.Sprintf("%s violates the '%s' rule", err.Field, err.Tag)
	}
}

func Authorizer() gin.HandlerFunc {
	return func(c *gin.Context) {
		log := c.MustGet("log").(*health.Job)

		accessToken := strings.TrimPrefix(c.GetHeader("Authorization"), "Bearer ")
		if accessToken == "" {
			c.AbortWithStatus(http.StatusUnauthorized)
			return
		}

		// Are the user's claims valid?
		claims, err := helper.ExtractUserClaims(accessToken)
		if err != nil {
			log.EventErr("extract_user_claims", err)
			c.AbortWithStatus(http.StatusUnauthorized)
			return
		}
		if claims.Origin != c.GetHeader("Origin") || claims.IPAddr != c.ClientIP() {
			log.EventKv("validate_user_claims", health.Kvs{
				"actualOrigin":   c.GetHeader("Origin"),
				"expectedOrigin": claims.Origin,
				"actualIP":       c.ClientIP(),
				"expectedIP":     claims.IPAddr,
			})
			c.AbortWithStatus(http.StatusUnauthorized)
			return
		}

		// Has the token expired?
		expiryTime := time.Unix(0, claims.Expiry)
		if time.Now().UTC().After(expiryTime) {
			c.AbortWithStatus(http.StatusUnauthorized)
			return
		}

		c.Set("userClaims", claims)
		c.Next()
	}
}
