package gin

import (
	"net/http"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/gocraft/health"
	"gitlab.com/bowheadsqg/openidein/api/pkg/core"
	"gitlab.com/bowheadsqg/openidein/api/pkg/core/gocql"
	"gitlab.com/bowheadsqg/openidein/api/pkg/core/helper"
	"golang.org/x/crypto/bcrypt"
)

func NewTokenController() *TokenController {
	return &TokenController{}
}

type TokenController struct{}

// TODO: Make GenerateAccessToken shorter.
func (TokenController) GenerateAccessToken(c *gin.Context) {
	log := c.MustGet("log").(*health.Job)

	accessToken := strings.TrimPrefix(c.GetHeader("Authorization"), "Bearer ")
	if accessToken != "" {
		claims, err := helper.ExtractUserClaims(accessToken)
		if err != nil {
			log.EventErr("extract_user_claims", err)
			c.Set("response", gin.H{
				"status":  http.StatusInternalServerError,
				"message": "We cannot authenticate you at the moment. Please try again later.",
			})
			return
		}
		if claims.Origin != c.GetHeader("Origin") || claims.IPAddr != c.ClientIP() {
			log.EventKv("validate_user_claims", health.Kvs{
				"actualOrigin":   c.GetHeader("Origin"),
				"expectedOrigin": claims.Origin,
				"actualIP":       c.ClientIP(),
				"expectedIP":     claims.IPAddr,
			})
			c.AbortWithStatus(http.StatusUnauthorized)
			return
		}
		expiryTime, renewTime := time.Unix(0, claims.Expiry), time.Unix(0, claims.Renew)

		now := time.Now().UTC()
		if now.After(expiryTime) && now.Before(renewTime) {
			newAccessToken, validity, err := helper.GenAccessToken(claims.UserID,
				claims.Origin, claims.IPAddr)
			if err != nil {
				log.EventErr("renew_access_token", err)
				c.Set("response", gin.H{
					"status":  http.StatusInternalServerError,
					"message": "We cannot authenticate you at the moment. Please try again later.",
				})
				return
			}
			c.Set("response", gin.H{
				"status": http.StatusOK,
				"data":   gin.H{"accessToken": newAccessToken, "expire": validity},
			})
			return
		}
	}

	var creds core.UserCredentials
	if err := c.ShouldBindJSON(&creds); err != nil {
		c.Set("validationError", err)
		return
	}

	// Fetch saved credentials.
	userCredsService := gocql.NewUserCredentialsService(gocql.DefaultCluster())
	savedCreds, err := userCredsService.One(creds.Username)
	if err != nil {
		log.EventErr("fetch_user_credentials", err)
		c.Set("response", gin.H{
			"status":  http.StatusInternalServerError,
			"message": "We cannot authenticate you at the moment. Please try again later.",
		})
		return
	}
	if savedCreds == nil {
		log.EventKv("wrong_username", health.Kvs{
			"client_ip": c.ClientIP(),
			"username":  creds.Username,
		})
		c.Set("response", gin.H{
			"status":  http.StatusUnauthorized,
			"message": "Your username/password is wrong. Please try again.",
		})
		return
	}

	// Validate password.
	invalid := bcrypt.CompareHashAndPassword(
		[]byte(savedCreds.Password),
		[]byte(creds.Password),
	)
	if invalid != nil {
		log.EventKv("wrong_password", health.Kvs{
			"client_ip": c.ClientIP(),
			"username":  creds.Username,
		})
		c.Set("response", gin.H{
			"status":  http.StatusUnauthorized,
			"message": "Your username/password is wrong. Please try again.",
		})
		return
	}

	accessToken, validity, err := helper.GenAccessToken(savedCreds.UserID,
		c.GetHeader("Origin"), c.ClientIP())
	if err != nil {
		log.EventErr("generate_access_token_for_user", err)
		c.Set("response", gin.H{
			"status":  http.StatusInternalServerError,
			"message": "We cannot authenticate you at the moment. Please try again later.",
		})
		return
	}

	c.Set("response", gin.H{
		"status": http.StatusOK,
		"data":   gin.H{"accessToken": accessToken, "expire": validity},
	})
}
