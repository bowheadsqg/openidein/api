package gin

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/gocraft/health"
	"gitlab.com/bowheadsqg/openidein/api/pkg/core"
	"gitlab.com/bowheadsqg/openidein/api/pkg/core/gocql"
)

func NewUserController() *UserController {
	return &UserController{}
}

type UserController struct{}

func (UserController) CreateUser(c *gin.Context) {
	log := c.MustGet("log").(*health.Job)

	var user core.User
	if err := c.ShouldBindJSON(&user); err != nil {
		c.Set("validationError", err)
		return
	}

	userService := gocql.NewUserService(gocql.DefaultCluster())
	userID, err := userService.Create(user)
	if err != nil {
		log.EventErr("save_user", err)
		c.Set("response", gin.H{
			"status":  http.StatusInternalServerError,
			"message": "Registration failed",
		})
		return
	}

	c.Set("response", gin.H{
		"status": http.StatusOK,
		"data":   userID,
	})
}
