// Package viper provides access to static and environment configuration values.
package viper

import (
	"errors"
	"path/filepath"
	"strings"

	"github.com/spf13/viper"
	"gitlab.com/bowheadsqg/openidein/api/pkg/config"
)

var defaultConfig config.Config

// Default returns the default configuration object.
func Default() config.Config {
	if defaultConfig == nil {
		panic(errors.New("default config not set"))
	}
	return defaultConfig
}

// SetDefault replaces the default configuration object.
func SetDefault(c config.Config) {
	defaultConfig = c
}

// Config wraps viper.Viper.
type Config struct {
	*viper.Viper
}

// New returns a new configuration object by reading config values from a source.
func New(v *viper.Viper, path string) *Config {
	c := &Config{v}

	if path != "" {
		name := strings.Split(filepath.Base(path), ".")[0] // filename w/o ext
		dir := filepath.Dir(path)

		c.SetConfigName(name)
		c.SetConfigType("yaml")
		c.AddConfigPath(dir)
		err := c.ReadInConfig()
		if err != nil {
			panic(err)
		}
	} else {
		c.SetEnvPrefix(config.EnvPrefix)
		c.AutomaticEnv()
	}
	return c
}

// Port returns the port used by the web server.
func (c *Config) Port() string {
	c.SetDefault("port", "8000")
	return c.GetString("port")
}

// LogPath returns relative log file path appended to project path.
func (c *Config) LogPath() string {
	logPath := c.GetString("log_path")
	if logPath == "" {
		panic(errors.New("no log path supplied"))
	}
	return logPath
}

// TokenKey returns a base64-encoded token key. It also optionally set a new value.
func (c *Config) TokenKey() string {
	tokenKey := c.GetString("token_key")
	if tokenKey == "" {
		panic(errors.New("no token key supplied"))
	}
	return tokenKey
}

// CassandraClusterIPs returns an array of Cassandra node IP addresses.
func (c *Config) CassandraClusterIPs() []string {
	clusterIPs := c.GetStringSlice("cassandra_cluster_ips")
	if len(clusterIPs) == 0 {
		panic(errors.New("no Cassandra node IP addresses supplied"))
	}
	return clusterIPs
}

// CassandraKeyspace returns the Cassandra active keyspace name.
func (c *Config) CassandraKeyspace() string {
	keyspace := c.GetString("cassandra_keyspace")
	if keyspace == "" {
		panic(errors.New("no Cassandra keyspace supplied"))
	}
	return keyspace
}
