package viper_test

import (
	"os"
	"testing"

	"github.com/spf13/viper"
	"gitlab.com/bowheadsqg/openidein/api/pkg/config"
	myconfig "gitlab.com/bowheadsqg/openidein/api/pkg/core/viper"
	"gotest.tools/assert"
)

func TestReadTokenKeyFromConfig(t *testing.T) {
	os.Setenv(config.EnvPrefix+"_TOKEN_KEY", "dGVzdA==")
	cfg := myconfig.New(viper.New(), "")
	myconfig.SetDefault(cfg)

	keyReader := myconfig.NewTokenKeyReader()
	key, err := keyReader.Read()
	assert.NilError(t, err)
	assert.Equal(t, string(key), "test")
}
