package viper_test

import (
	"io/ioutil"
	"os"
	"path/filepath"
	"testing"

	"github.com/spf13/viper"
	"gitlab.com/bowheadsqg/openidein/api/pkg/config"
	myconfig "gitlab.com/bowheadsqg/openidein/api/pkg/core/viper"
	"gotest.tools/assert"
)

func TestSetGetDefaultConfig(t *testing.T) {
	c := myconfig.New(viper.New(), "")
	myconfig.SetDefault(c)

	//opt := cmpopts.IgnoreUnexported(viper.Viper{})
	//if !cmp.Equal(c, myconfig.Default(), opt) {
	//	tester.Unexpected(t, c, myconfig.Default())
	//}
	assert.Equal(t, myconfig.Default(), c)
}

func TestDefaultConfigNotSet(t *testing.T) {
	defer func() {
		r := recover()
		assert.Assert(t, r != nil)
		assert.Error(t, r.(error), "default config not set")
	}()
	myconfig.SetDefault(nil)
	_ = myconfig.Default()
}

func TestNewConfigPathSupplied(t *testing.T) {
	dir, err := ioutil.TempDir("", "")
	assert.NilError(t, err)

	cfgPath := filepath.Join(dir, "config.yaml")
	err = ioutil.WriteFile(cfgPath, []byte(`port: "9999"`), 0600)
	assert.NilError(t, err)

	defer os.Remove(cfgPath)

	c := myconfig.New(viper.New(), cfgPath)
	// Assume configured port in myconfig.yaml is 9999.
	assert.Equal(t, c.Port(), "9999")
}

func TestNewConfigNoPathSupplied(t *testing.T) {
	os.Setenv(config.EnvPrefix+"_PORT", "1234")
	c := myconfig.New(viper.New(), "")
	assert.Equal(t, c.Port(), "1234")
}

func TestNewConfigReadFileError(t *testing.T) {
	defer func() {
		r := recover()
		assert.Assert(t, r != nil)
		assert.Error(t, r.(error), `Config File "config" Not Found in "[/any/path]"`)
	}()
	_ = myconfig.New(viper.New(), "/any/path/config.yaml")
}

func TestGetDefaultPort(t *testing.T) {
	os.Setenv(config.EnvPrefix+"_PORT", "")
	c := myconfig.New(viper.New(), "")
	assert.Equal(t, c.Port(), "8000")
}

func TestGetLogPath(t *testing.T) {
	os.Setenv(config.EnvPrefix+"_LOG_PATH", "/test/path")
	c := myconfig.New(viper.New(), "")
	assert.Equal(t, c.LogPath(), "/test/path")
}

func TestGetLogPathNotSupplied(t *testing.T) {
	defer func() {
		r := recover()
		assert.Assert(t, r != nil)
		assert.Error(t, r.(error), "no log path supplied")
	}()
	os.Setenv(config.EnvPrefix+"_LOG_PATH", "")
	c := myconfig.New(viper.New(), "")
	_ = c.LogPath()
}

func TestGetTokenKeyNotSupplied(t *testing.T) {
	defer func() {
		r := recover()
		assert.Assert(t, r != nil)
		assert.Error(t, r.(error), "no token key supplied")
	}()
	os.Setenv(config.EnvPrefix+"_TOKEN_KEY", "")
	c := myconfig.New(viper.New(), "")
	_ = c.TokenKey()
}

func TestGetCassandraClusterIPs(t *testing.T) {
	os.Setenv(config.EnvPrefix+"_CASSANDRA_CLUSTER_IPS", "127.0.0.1")
	c := myconfig.New(viper.New(), "")
	assert.DeepEqual(t, c.CassandraClusterIPs(), []string{"127.0.0.1"})
}

func TestGetCassandraClusterIPsNotSupplied(t *testing.T) {
	defer func() {
		r := recover()
		assert.Assert(t, r != nil)
		assert.Error(t, r.(error), "no Cassandra node IP addresses supplied")
	}()
	os.Setenv(config.EnvPrefix+"_CASSANDRA_CLUSTER_IPS", "")
	c := myconfig.New(viper.New(), "")
	_ = c.CassandraClusterIPs()
}

func TestGetCassandraKeyspace(t *testing.T) {
	os.Setenv(config.EnvPrefix+"_CASSANDRA_KEYSPACE", "openidein")
	c := myconfig.New(viper.New(), "")
	assert.Equal(t, c.CassandraKeyspace(), "openidein")
}

func TestGetCassandraKeyspaceNotSupplied(t *testing.T) {
	defer func() {
		r := recover()
		assert.Assert(t, r != nil)
		assert.Error(t, r.(error), "no Cassandra keyspace supplied")
	}()
	os.Setenv(config.EnvPrefix+"_CASSANDRA_KEYSPACE", "")
	c := myconfig.New(viper.New(), "")
	_ = c.CassandraKeyspace()
}
