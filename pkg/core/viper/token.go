package viper

import (
	"encoding/base64"

	"gitlab.com/bowheadsqg/openidein/api/pkg/core"
)

// TokenKeyReader implements core.TokenKeyReader interface.
type TokenKeyReader struct{}

// NewTokenKeyReader returns a new reader ready to read from a config variable.
func NewTokenKeyReader() *TokenKeyReader {
	return &TokenKeyReader{}
}

// Read reads a base64-encoded value from config and decode it
// into an array of bytes, which is the token key itself.
func (r *TokenKeyReader) Read() (core.TokenKey, error) {
	return base64.StdEncoding.DecodeString(Default().TokenKey())
}
