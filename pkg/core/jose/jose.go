// Package jose implements access tokens as JWT (JSON Web Token).
package jose

import (
	"github.com/dvsekhvalnov/jose2go"
	"gitlab.com/bowheadsqg/openidein/api/pkg/core"
)

// Token implements core.Token interface.
type Token struct {
	val string
}

// NewToken returns a new JWT struct which contains the JWT string.
func NewToken(val string) *Token {
	return &Token{val}
}

func (t Token) Value() string {
	return t.val
}

// Extract decodes the JWT string into a token payload struct.
func (t Token) Extract(keyReader core.TokenKeyReader, payload core.TokenPayload) error {
	key, err := keyReader.Read()
	if err != nil {
		return err
	}

	payloadVal, _, err := jose.Decode(t.val, []byte(key))
	if err != nil {
		return err
	}
	return payload.FromString(payloadVal) // in-place substitution
}

// TokenProtection implements core.TokenProtection interface.
type TokenProtection struct {
	payload core.TokenPayload
}

// NewTokenProtection returns a new payload protector.
func NewTokenProtection(payload core.TokenPayload) *TokenProtection {
	return &TokenProtection{payload}
}

// Do performs encrypt+sign or sign only, depending on the selected algorithms,
// to produce a new JWT.
func (tp TokenProtection) Do(algo core.TokenAlgo, keyReader core.TokenKeyReader) (core.Token, error) {
	key, err := keyReader.Read()
	if err != nil {
		return nil, err
	}
	payloadStr, err := tp.payload.ToString()
	if err != nil {
		return nil, err
	}

	jwt, err := jose.Encrypt(payloadStr, algo.KeyManagement(), algo.Encryption(),
		[]byte(key), jose.Header("typ", "JWT"))
	if err != nil {
		return nil, err
	}
	return NewToken(jwt), nil
}

// TokenAlgo implements core.TokenAlgo interface.
type TokenAlgo struct{}

// NewTokenAlgo returns a set of algorithms for JWT creation.
func NewTokenAlgo() *TokenAlgo {
	return new(TokenAlgo)
}

func (TokenAlgo) Signature() string {
	return jose.HS256
}

func (TokenAlgo) KeyManagement() string {
	return jose.A256GCMKW
}

func (TokenAlgo) Encryption() string {
	return jose.A128GCM
}
