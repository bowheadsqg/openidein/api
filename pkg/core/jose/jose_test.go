package jose_test

import (
	"errors"
	"testing"

	jose2go "github.com/dvsekhvalnov/jose2go"
	"gitlab.com/bowheadsqg/openidein/api/pkg/core"
	"gitlab.com/bowheadsqg/openidein/api/pkg/core/jose"
	"gitlab.com/bowheadsqg/openidein/api/pkg/mock"
	"gotest.tools/assert"
)

func TestProtectExtractToken(t *testing.T) {
	expectedJsonStr := `{"name":"josh","age":22}`

	// Protect payload as token.
	keyReader := mock.NewTokenKeyReader()
	keyReader.ReadFn = func() (core.TokenKey, error) {
		return make([]byte, 32), nil // 32 bytes because of 256-bit algorithms used
	}
	payload := mock.NewTokenPayload()
	payload.ToStringFn = func() (string, error) {
		return expectedJsonStr, nil
	}

	protection := jose.NewTokenProtection(payload)
	token, err := protection.Do(jose.NewTokenAlgo(), keyReader)
	assert.NilError(t, err)
	assert.Equal(t, keyReader.ReadInvoked, true)
	assert.Equal(t, payload.ToStringInvoked, true)

	// Extract payload from token.
	var actualJsonStr string
	payload.FromStringFn = func(s string) error {
		actualJsonStr = s
		return nil
	}
	err = token.Extract(keyReader, payload)
	assert.NilError(t, err)
	assert.Equal(t, payload.FromStringInvoked, true)
	assert.Equal(t, actualJsonStr, expectedJsonStr)
}

func TestProtectTokenInvalidKeySize(t *testing.T) {
	keyReader := mock.NewTokenKeyReader()
	keyReader.ReadFn = func() (core.TokenKey, error) {
		return make([]byte, 16), nil // should be 32 bytes
	}
	payload := mock.NewTokenPayload()
	payload.ToStringFn = func() (string, error) {
		return `{"name":"josh","age":22}`, nil
	}

	protection := jose.NewTokenProtection(payload)
	_, err := protection.Do(jose.NewTokenAlgo(), keyReader)
	assert.Equal(t, keyReader.ReadInvoked, true)
	assert.Equal(t, payload.ToStringInvoked, true)
	assert.Error(t, err, "AesGcmKW.WrapNewKey(): expected key of "+
		"size 256 bits, but was given 128 bits.")
}

func TestProtectTokenReadKeyError(t *testing.T) {
	keyReader := mock.NewTokenKeyReader()
	keyReader.ReadFn = func() (core.TokenKey, error) {
		return nil, errors.New("oh no")
	}
	payload := mock.NewTokenPayload()

	protection := jose.NewTokenProtection(payload)
	_, err := protection.Do(jose.NewTokenAlgo(), keyReader)
	assert.Equal(t, keyReader.ReadInvoked, true)
	assert.Error(t, err, "oh no")
}

func TestProtectTokenPayloadStringError(t *testing.T) {
	keyReader := mock.NewTokenKeyReader()
	keyReader.ReadFn = func() (core.TokenKey, error) {
		return make([]byte, 32), nil
	}
	payload := mock.NewTokenPayload()
	payload.ToStringFn = func() (string, error) {
		return "", errors.New("oh no")
	}

	protection := jose.NewTokenProtection(payload)
	_, err := protection.Do(jose.NewTokenAlgo(), keyReader)
	assert.Equal(t, keyReader.ReadInvoked, true)
	assert.Equal(t, payload.ToStringInvoked, true)
	assert.Error(t, err, "oh no")
}

func TestExtractReadKeyError(t *testing.T) {
	keyReader := mock.NewTokenKeyReader()
	keyReader.ReadFn = func() (core.TokenKey, error) {
		return nil, errors.New("oh no")
	}
	payload := mock.NewTokenPayload()

	token := jose.NewToken("dummy")
	err := token.Extract(keyReader, payload)
	assert.Equal(t, keyReader.ReadInvoked, true)
	assert.Error(t, err, "oh no")
}

func TestExtractDecodeError(t *testing.T) {
	keyReader := mock.NewTokenKeyReader()
	keyReader.ReadFn = func() (core.TokenKey, error) {
		return make([]byte, 2), nil // illegal base64
	}
	payload := mock.NewTokenPayload()

	token := jose.NewToken("dummy")
	err := token.Extract(keyReader, payload)
	assert.Equal(t, keyReader.ReadInvoked, true)
	assert.Error(t, err, "illegal base64 data at input byte 4")
}

func TestGetTokenValue(t *testing.T) {
	token := jose.NewToken("dummy")
	assert.Equal(t, token.Value(), "dummy")
}

// TestCorrectTokenAlgo ensures that we double check the algorithms we use for tokens.
func TestCorrectTokenAlgo(t *testing.T) {
	algo := jose.NewTokenAlgo()
	assert.Equal(t, algo.Signature(), jose2go.HS256)
	assert.Equal(t, algo.KeyManagement(), jose2go.A256GCMKW)
	assert.Equal(t, algo.Encryption(), jose2go.A128GCM)
}
