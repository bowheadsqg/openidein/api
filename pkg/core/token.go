package core

type Token interface {
	Value() string
	Extract(keyReader TokenKeyReader, payload TokenPayload) error
}

type TokenProtection interface {
	Do(TokenAlgo, TokenKeyReader) (Token, error)
}

type TokenPayload interface {
	Value() interface{}
	FromString(string) error
	ToString() (string, error)
}

type TokenAlgo interface {
	Signature() string
	KeyManagement() string
	Encryption() string
}

type TokenKeyReader interface {
	Read() (TokenKey, error)
}

type TokenKey []byte
